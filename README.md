Prueba Técnica Siroko Backend
========================

La prueba técnica del backend de "Siroko" consiste en una apì que expone diferentes ep para el 
manejo de productos y carrito de la compra. Se exponen los siguientes:

- Para añadir producto al carrito:
PATCH: http://api.siroko.local:8888/product/add
- Para eliminar producto del carrito:
PATCH: http://api.siroko.local:8888/product/remove
- Para cambiar el número del producto en el carrito:
PATCH: http://api.siroko.local:8888/product/change
- Para comprar los productos del carrito de la compra:
DELETE: http://api.siroko.local:8888/checkout


La prueba se ha desarrollado en symfony 6.3.4 siguiendo un modelo de arquitectura 
hexagonal con DDD.

Requerimientos
------------

  * Docker;
  * y los requerimientos de una aplicación symfony 6.

Instalación
------------

* Ejecutar en el directorio raíz de la aplicación el siguiente comando para
instalar los contenedores dockers necesarios para correr la aplicación:

```bash
$ make install
```

**Importante:** Editar el archivo /etc/hosts y añadir la siguiente linea

```bash
127.0.0.1	api.siroko.local
```

Para crear la base de datos y añadir los datafixture basta con ejecutar en la raíz del proyecto el siguiente 
comando:

```bash
make db-create
```

Será necesario enlazar la base de datos de nuestro proyecto con el docker de mysql. Para
este ejemplo de prueba técnica (aunque en un proyecto real jamás se debe de subir la configuración
del proyecto local al .env del proyecto) he subido la configuración de la BBDD al .env Un ejemplo:

```bash
DATABASE_URL="mysql://siroko_user:1234@db:3306/siroko?serverVersion=8&charset=utf8mb4"
```

**Nota:** Nuestra aplicación será expuesta a través del puerto 8888.

Uso
-----
A partir de ahora la API debería de ser 100% operativa desde el front.
Se ha enviado un front con una UI con la que poder manejar todos los eps mencionados.

Tests
-----

Se han creado test para todos los casos de uso y se podrán ejecutar con el comando:

```bash
make api-tests
```

Contacto
-----

Cualquier duda de instalación o uso de la aplicación la puedes consultar en 
los siguientes contactos:

* Télefono: 655727029
* Correo: jose.labrador.gonzalez@gmail.com
