<?php

declare(strict_types=1);

namespace App\Tests\Factories;

use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\ProductId;
use App\Siroko\Api\Domain\Model\Products\Status;

class ProductsFactory
{
    public function __construct()
    {
    }

    public static function getProductPublished(): Product
    {
        return Product::create(
            new ProductId('id-prueba-caca'.time()),
            'Camiseta de deporte',
            'Descripción de la camiseta de deporte',
            Status::PUBLISHED,
            0
        );
    }

    public static function getProductPending(): Product
    {
        return Product::create(
            new ProductId('id-prueba-'.time()),
            'Camiseta de deporte',
            'Descripción de la camiseta de deporte',
            Status::PENDING,
            0
        );
    }

    public static function getProductRemove(): Product
    {
        return Product::create(
            new ProductId('id-prueba-'.time()),
            'Camiseta de deporte',
            'Descripción de la camiseta de deporte',
            Status::REMOVED,
            0
        );
    }

    public static function getProductInvalidStatus(): Product
    {
        return Product::create(
            new ProductId('id-prueba-'.time()),
            'Camiseta de deporte',
            'Descripción de la camiseta de deporte',
            'invalid status',
            0
        );
    }
}
