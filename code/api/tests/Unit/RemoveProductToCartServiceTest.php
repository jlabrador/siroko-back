<?php

declare(strict_types=1);

namespace tests\App\Tests\Unit;

use App\Siroko\Api\Application\Command\RemoveProductToCartHandler;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use App\Siroko\Api\Domain\Model\Products\InvalidRemoveNumberCartException;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\Status;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\Repository\InMemoryProductRepository;

class RemoveProductToCartServiceTest extends KernelTestCase
{
    private Product $product;
    private RemoveProductToCartHandler $removeProductToCartHandler;
    private GetProductRequest $getProductRequest;

    private InMemoryProductRepository $productRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->productRepository = new InMemoryProductRepository();
        $products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        /** @var Product $product */
        $this->product = $products->first();
        $this->getProductRequest = new GetProductRequest(
            $this->product->getId()->getValue()
        );
        $this->removeProductToCartHandler = new RemoveProductToCartHandler(
            $this->productRepository
        );
    }

    public function test_remove_product_to_cart_is_successful()
    {
        $this->productRepository->addNumberCart($this->product->getId()->getValue());
        ($this->removeProductToCartHandler)($this->getProductRequest);
        $productAfterAddToCart = $this->productRepository->findById($this->product->getId()->getValue());
        $numProductInCartAfter = $productAfterAddToCart->getNumberCart();
        $this->assertEquals(0, $numProductInCartAfter);
    }

    public function test_exception_remove_product_to_cart_with_value_less_0()
    {
        $this->expectException(InvalidRemoveNumberCartException::class);
        ($this->removeProductToCartHandler)($this->getProductRequest);
    }
}
