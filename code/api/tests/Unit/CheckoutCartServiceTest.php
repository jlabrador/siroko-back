<?php

declare(strict_types=1);

namespace tests\App\Tests\Unit;

use App\Siroko\Api\Application\Command\CheckoutCartHandler;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\ProductCollection;
use App\Siroko\Api\Domain\Model\Products\Status;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\Repository\InMemoryProductRepository;

class CheckoutCartServiceTest extends KernelTestCase
{
    private ProductCollection $products;
    private CheckoutCartHandler $checkoutCartHandler;
    private InMemoryProductRepository $productRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->productRepository = new InMemoryProductRepository();
        $this->products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        /** @var Product $product */
        foreach ($this->products->getCollection() as $product) {
            $this->productRepository->addNumberCart($product->getId()->getValue());
        }
        $this->products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));

        $this->checkoutCartHandler = new CheckoutCartHandler(
            $this->productRepository
        );
    }

    public function test_add_product_to_cart_is_successful()
    {
        ($this->checkoutCartHandler)();
        $productsBeforeCheckoutCart = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        /** @var Product $product */
        foreach ($productsBeforeCheckoutCart->getCollection() as $product) {
            $this->assertEquals(0, $product->getNumberCart());
        }
    }

}
