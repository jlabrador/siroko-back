<?php

declare(strict_types=1);

namespace tests\App\Tests\Unit;

use App\Siroko\Api\Application\Query\Products\GetProductsHandler;
use App\Siroko\Api\Application\Request\Products\GetProductsRequest;
use App\Siroko\Api\Domain\Model\Products\ProductCollection;
use App\Siroko\Api\Domain\Model\Products\Status;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\Repository\InMemoryProductRepository;

class GetProductsServiceTest extends KernelTestCase
{
    private ProductCollection $products;
    private GetProductsHandler $getProductsHandler;
    private InMemoryProductRepository $productRepository;
    private GetProductsRequest $getProductsRequest;

    public function setUp(): void
    {
        parent::setUp();

        $this->productRepository = new InMemoryProductRepository();


        $this->getProductsHandler = new GetProductsHandler(
            $this->productRepository
        );

        $this->getProductsRequest = new GetProductsRequest(Status::PUBLISHED);
    }

    public function test_add_product_to_cart_is_successful()
    {
        $getProductResponse = ($this->getProductsHandler)($this->getProductsRequest);
        $this->assertEquals(2, count($getProductResponse->toArray()));
    }

}
