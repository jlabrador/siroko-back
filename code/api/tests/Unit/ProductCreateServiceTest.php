<?php

declare(strict_types=1);

namespace tests\App\Tests\Unit;

use App\Siroko\Api\Application\Command\CreateProductHandler;
use App\Siroko\Api\Application\Request\Products\CreateProductRequest;
use App\Siroko\Api\Domain\Model\Products\InvalidStatusValueException;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\Status;
use App\Siroko\Api\Infrastructure\Service\UuidIdStringGenerator;
use App\Tests\Factories\ProductsFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\Repository\InMemoryProductRepository;

class ProductCreateServiceTest extends KernelTestCase
{

    private Product $product;
    private CreateProductRequest $createProductRequest;
    private CreateProductHandler $createProductHandler;

    private InMemoryProductRepository $productRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->product = ProductsFactory::getProductPublished();
        $this->createProductRequest = new CreateProductRequest(
            $this->product->getTitle(),
            $this->product->getDescription(),
            $this->product->getStatus()->getValue()
        );
        $this->productRepository = new InMemoryProductRepository();
        $this->createProductHandler = new CreateProductHandler(
            $this->productRepository,
            new UuidIdStringGenerator()
        );
    }

    public function test_create_new_product_with_not_null_product_is_successful()
    {
        $products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        $numProductsInsertBefore = $products->count();
        ($this->createProductHandler)($this->createProductRequest);
        $products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        $numProductsInsertAfter = $products->count();
        $this->assertEquals($numProductsInsertBefore + 1, $numProductsInsertAfter);
    }

    public function test_create_new_asset_with_status_invalid_created_uid_returns_an_exception()
    {
        $this->expectException(InvalidStatusValueException::class);
        ProductsFactory::getProductInvalidStatus();
    }

}
