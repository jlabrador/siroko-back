<?php

declare(strict_types=1);

namespace tests\App\Tests\Unit;

use App\Siroko\Api\Application\Command\AddProductToCartHandler;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\Status;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\Repository\InMemoryProductRepository;

class AddProductToCartServiceTest extends KernelTestCase
{
    private Product $product;
    private AddProductToCartHandler $addProductToCartHandler;
    private GetProductRequest $getProductRequest;

    private InMemoryProductRepository $productRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->productRepository = new InMemoryProductRepository();
        $products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        /** @var Product $product */
        $this->product = $products->first();
        $this->getProductRequest = new GetProductRequest(
            $this->product->getId()->getValue()
        );
        $this->addProductToCartHandler = new AddProductToCartHandler(
            $this->productRepository
        );
    }

    public function test_add_product_to_cart_is_successful()
    {
        $numProductInCartBefore = $this->product->getNumberCart();
        ($this->addProductToCartHandler)($this->getProductRequest);
        $productAfterAddToCart = $this->productRepository->findById($this->product->getId()->getValue());
        $numProductInCartAfter = $productAfterAddToCart->getNumberCart();
        $this->assertEquals($numProductInCartBefore + 1, $numProductInCartAfter);
    }

}
