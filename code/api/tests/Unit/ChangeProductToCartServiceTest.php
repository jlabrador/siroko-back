<?php

declare(strict_types=1);

namespace tests\App\Tests\Unit;

use App\Siroko\Api\Application\Command\AddProductToCartHandler;
use App\Siroko\Api\Application\Command\ChangeProductToCartHandler;
use App\Siroko\Api\Application\Request\Products\ChangeProductNumberCartRequest;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\Status;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\Repository\InMemoryProductRepository;

class ChangeProductToCartServiceTest extends KernelTestCase
{
    private Product $product;
    private ChangeProductToCartHandler $changeProductToCartHandler;
    private ChangeProductNumberCartRequest $changeProductNumberCartRequest;

    private InMemoryProductRepository $productRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->productRepository = new InMemoryProductRepository();
        $products = $this->productRepository->findByStatus(new Status(Status::PUBLISHED));
        /** @var Product $product */
        $this->product = $products->first();
        $this->changeProductNumberCartRequest = new ChangeProductNumberCartRequest(
            $this->product->getId()->getValue(),
            6
        );
        $this->changeProductToCartHandler = new ChangeProductToCartHandler(
            $this->productRepository
        );
    }

    public function test_add_product_to_cart_is_successful()
    {
        $productChangeNumberCart = $this->productRepository->changeNumberCart(
            $this->changeProductNumberCartRequest->getUid(),
            $this->changeProductNumberCartRequest->getValueNumberCart()
        );
        $this->assertEquals(6, $productChangeNumberCart->getNumberCart());
    }

}
