<?php


namespace App\Tests\Repository;


use App\Siroko\Api\Application\Command\ProductNotFoundException;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\ProductCollection;
use App\Siroko\Api\Domain\Model\Products\ProductId;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;
use App\Siroko\Api\Domain\Model\Products\Status;
use App\Siroko\Api\Infrastructure\Persistence\Doctrine\Entity\Product as ProductEntity;
use App\Siroko\Api\Infrastructure\Service\UuidIdStringGenerator;

class InMemoryProductRepository implements ProductRepository
{
    private array $products;

    public function __construct()
    {
        $idStringGenerator = new UuidIdStringGenerator();
        $productId1 = $idStringGenerator->generate();
        $this->products[$productId1] = Product::create(
            new ProductId($productId1),
            'Camiseta de deporte publicada',
            'Descripción de la camiseta de deporte publicada',
            Status::PUBLISHED
        );

        $productId2 = $idStringGenerator->generate();
        $this->products[$productId2] = Product::create(
            new ProductId($productId2),
            'Camiseta de deporte publicada',
            'Descripción de la camiseta de deporte publicada',
            Status::PUBLISHED
        );

        $productId3 = $idStringGenerator->generate();
        $this->products[$productId3] = Product::create(
            new ProductId($productId3),
            'Camiseta de deporte publicada',
            'Descripción de la camiseta de deporte pendiente de publicación',
            Status::PENDING
        );

        $productId4 = $idStringGenerator->generate();
        $this->products[$productId4] = Product::create(
            new ProductId($productId4),
            'Camiseta de deporte borrada',
            'Descripción de la camiseta de deporte borrada',
            Status::REMOVED
        );
    }

    public function insert(Product $product): void
    {
        $this->products[] = Product::create(
            new ProductId($product->getId()->getValue()),
            $product->getTitle(),
            $product->getDescription(),
            $product->getStatus()->getValue(),
        );
    }


    public function findByStatus(Status $status): ProductCollection
    {
        $productCollection = new ProductCollection();

        /** @var Product $product */
        foreach ($this->products as $product) {
            if ($product->getStatus()->getValue() === Status::PUBLISHED) {
                $productCollection->add($product);
            }
        }

        return $productCollection;
    }
    public function findById(string $uid): Product
    {
        /** @var Product $product */
        foreach ($this->products as $product) {
            if ($product->getId()->getValue() === $uid) {
                return $product;
            }
        }

        throw new ProductNotFoundException('Product id '.$uid.' not exist');
    }

    public function addNumberCart(string $uid): Product
    {
        $product = $this->findById($uid);
        $productEntity = ProductEntity::fromDomain($product);
        $productEntity->addNumberCart();
        $this->products[$uid] = $productEntity->toDomain();
        return $this->products[$uid];
    }

    public function changeNumberCart(string $uid, int $value): Product
    {
        $product = $this->findById($uid);
        $productEntity = ProductEntity::fromDomain($product);
        $productEntity->setNumberCart($value);
        $this->products[$uid] = $productEntity->toDomain();
        return $this->products[$uid];
    }

    public function removeNumberCart(string $uid): Product
    {
        $product = $this->findById($uid);
        $productEntity = ProductEntity::fromDomain($product);
        $productEntity->removeNumberCart();
        $this->products[$uid] = $productEntity->toDomain();
        return $this->products[$uid];
    }

    public function checkoutCart(): ProductCollection
    {
        $products = $this->findByStatus(new Status(Status::PUBLISHED));

        $productCollection = ProductCollection::init();

        if (!empty($products)) {
            /** @var Product $product */
            foreach ($products->getCollection() as $product) {
                if ($product->getNumberCart() > 0) {
                    $productEntity = ProductEntity::fromDomain($product);
                    $productEntity->resetNumberCart();
                    $productCollection->add($productEntity->toDomain());
                    $this->products[$product->getId()->getValue()] = $productEntity->toDomain();
                }
            }
        }

        return $productCollection;
    }



}