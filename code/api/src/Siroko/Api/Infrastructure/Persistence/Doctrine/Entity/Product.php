<?php

namespace App\Siroko\Api\Infrastructure\Persistence\Doctrine\Entity;

use App\Siroko\Api\Domain\Model\Products\InvalidRemoveNumberCartException;
use App\Siroko\Api\Domain\Model\Products\Product as ProductDomain;

class Product
{
    public function __construct(
        private string $id,
        private string $title,
        private string $description,
        private string $status,
        private int $numberCart,
        private \DateTimeInterface $createdAt,
        private ?\DateTimeInterface $updatedAt,
    )
    {
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getNumberCart(): int
    {
        return $this->numberCart;
    }

    public static function fromDomain(ProductDomain $product): self
    {
        return new self(
            $product->getId()->getValue(),
            $product->getTitle(),
            $product->getDescription(),
            $product->getStatus()->getValue(),
            $product->getNumberCart(),
            $product->getCreatedAt(),
            $product->getUpdatedAt(),
        );
    }

    public static function fromArray(array $product): self
    {
        return new self(
            $product['id'],
            $product['title'],
            $product['description'],
            $product['status'],
            $product['numberCart'],
            $product['createdAt'],
            $product['updatedAt'],
        );
    }

    public function toDomain(): ProductDomain
    {
        return ProductDomain::fromPrimitive(
            $this->getId(),
            $this->getTitle(),
            $this->getDescription(),
            $this->getStatus(),
            $this->getNumberCart(),
            $this->getCreatedAt(),
            $this->getUpdatedAt(),
        );
    }

    public function addNumberCart()
    {
        $this->numberCart ++;
        $this->refreshUpdatedAt();
    }

    public function removeNumberCart()
    {
        $this->numberCart --;
        $this->refreshUpdatedAt();

        if ($this->numberCart < 0) {
            throw new InvalidRemoveNumberCartException();
        }
    }

    public function setNumberCart(int $value)
    {
        if ($this->numberCart < 0) {
            throw new InvalidRemoveNumberCartException();
        }
        $this->refreshUpdatedAt();
        $this->numberCart = $value;
    }

    public function resetNumberCart()
    {
        $this->numberCart = 0;
        $this->refreshUpdatedAt();
    }

    private function refreshUpdatedAt()
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}