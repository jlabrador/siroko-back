<?php

namespace App\Siroko\Api\Infrastructure\Persistence\Doctrine\Repository;

use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\ProductCollection;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;
use App\Siroko\Api\Domain\Model\Products\Status;
use App\Siroko\Api\Infrastructure\Persistence\Doctrine\Entity\Product as ProductEntity;

class DoctrineProductRepository extends DoctrineRepository implements ProductRepository
{
    protected function entityClassName(): string
    {
        return ProductEntity::class;
    }

    public function findById(string $uid): Product
    {
        /** @var ProductEntity $product */
        $product = $this->repository->findOneBy([
            'id' => $uid
        ]);

        return $product->toDomain();
    }

    public function findByStatus(Status $status): ProductCollection
    {
        $products = $this->repository->findBy([
            'status' => $status->getValue()
        ]);
        
        $productCollection = ProductCollection::init();

        if (!empty($products)) {
            foreach ($products as $product) {
                $productCollection->add($product->toDomain());
            }
        }

        return $productCollection;
    }

    public function insert(Product $product): void
    {
        $this->entityManager->persist(
            ProductEntity::fromDomain($product)
        );

        $this->entityManager->flush();
    }

    public function addNumberCart(string $uid): Product
    {
        /** @var ProductEntity $product */
        $product = $this->repository->findOneBy([
            'id' => $uid
        ]);
        $product->addNumberCart();
        $this->entityManager->flush();
        return $product->toDomain();
    }

    public function removeNumberCart(string $uid): Product
    {
        /** @var ProductEntity $product */
        $product = $this->repository->findOneBy([
            'id' => $uid
        ]);
        $product->removeNumberCart();
        $this->entityManager->flush();
        return $product->toDomain();
    }

    public function checkoutCart(): ProductCollection
    {
        $products = $this->repository->findBy([
            'status' => Status::PUBLISHED
        ]);

        $productCollection = ProductCollection::init();

        if (!empty($products)) {
            /** @var ProductEntity $product */
            foreach ($products as $product) {
                if ($product->getNumberCart() > 0) {
                    $product->resetNumberCart();
                    $productCollection->add($product->toDomain());
                }
            }
        }

        $this->entityManager->flush();

        return $productCollection;
    }

    public function changeNumberCart(string $uid, int $value): Product
    {
        /** @var ProductEntity $product */
        $product = $this->repository->findOneBy([
            'id' => $uid
        ]);
        $product->setNumberCart($value);
        $this->entityManager->flush();
        return $product->toDomain();
    }

}