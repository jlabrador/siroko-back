<?php


namespace App\Siroko\Api\Infrastructure\Ui\Http\Controller\Carts;


use App\Siroko\Api\Application\Command\AddProductToCartHandler;
use App\Siroko\Api\Application\Command\CheckoutCartHandler;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use App\Siroko\Api\Application\Request\Products\GetProductsRequest;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CheckoutCartController
{
    private CheckoutCartHandler $checkoutCartHandler;

    public function __construct(CheckoutCartHandler $checkoutCartHandler)
    {
        $this->checkoutCartHandler = $checkoutCartHandler;
    }

    public function __invoke(Request $request)
    {
        try {
            $products = ($this->checkoutCartHandler)();
            $response = new JsonResponse([
                'status' => 'ok',
                'data' => $products->toArray()
            ]);
        } catch (Exception $e) {
            $response = new JsonResponse([
                'status' => 'error',
                'errorMesage' => $e->getMessage()
            ], 500);
        }

        return $response;
    }
}