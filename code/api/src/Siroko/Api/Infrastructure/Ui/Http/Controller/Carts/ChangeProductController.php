<?php


namespace App\Siroko\Api\Infrastructure\Ui\Http\Controller\Carts;


use App\Siroko\Api\Application\Command\AddProductToCartHandler;
use App\Siroko\Api\Application\Command\ChangeProductToCartHandler;
use App\Siroko\Api\Application\Request\Products\ChangeProductNumberCartRequest;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ChangeProductController
{
    private ChangeProductToCartHandler $changeProductToCartHandler;

    public function __construct(ChangeProductToCartHandler $changeProductToCartHandler)
    {
        $this->changeProductToCartHandler = $changeProductToCartHandler;
    }

    public function __invoke(Request $request)
    {
        try {
            $product = ($this->changeProductToCartHandler)(new ChangeProductNumberCartRequest(
                $request->get('uid'),
                (int) $request->get('numberCart')
            ));

            $response = new JsonResponse([
                'status' => 'ok',
                'data' => $product->toArray()
            ]);
        } catch (Exception $e) {
            $response = new JsonResponse([
                'status' => 'error',
                'errorMesage' => $e->getMessage()
            ], 500);
        }

        return $response;
    }
}