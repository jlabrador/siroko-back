<?php


namespace App\Siroko\Api\Infrastructure\Ui\Http\Controller\Carts;


use App\Siroko\Api\Application\Command\AddProductToCartHandler;
use App\Siroko\Api\Application\Command\RemoveProductToCartHandler;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use App\Siroko\Api\Domain\Model\Products\InvalidRemoveNumberCartException;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RemoveProductController
{
    private RemoveProductToCartHandler $removeProductToCartHandler;

    public function __construct(RemoveProductToCartHandler $removeProductToCartHandler)
    {
        $this->removeProductToCartHandler = $removeProductToCartHandler;
    }

    public function __invoke(Request $request)
    {
        try {
            $product = ($this->removeProductToCartHandler)(new GetProductRequest(
                $request->get('uid')
            ));

            $response = new JsonResponse([
                'status' => 'ok',
                'data' => $product->toArray()
            ]);
        } catch (InvalidRemoveNumberCartException $e) {
            $response = new JsonResponse([
                'status' => 'error',
                'errorMesage' => $e->getMessage()
            ], 500);
        }

        return $response;
    }
}