<?php

namespace App\Siroko\Api\Infrastructure\Ui\Http\Controller\Products;

use App\Siroko\Api\Application\Query\Products\GetProductsHandler;
use App\Siroko\Api\Application\Request\Products\GetProductsRequest;
use App\Siroko\Api\Domain\Model\Products\InvalidStatusValueException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GetProductsController
{
    private GetProductsHandler $getProductsHandler;

    /**
     * @param GetProductsHandler $getProductsHandler
     */
    public function __construct(GetProductsHandler $getProductsHandler)
    {
        $this->getProductsHandler = $getProductsHandler;
    }

    public function __invoke(Request $request)
    {
        try {
            $products = ($this->getProductsHandler)(
                new GetProductsRequest($request->get('status', 'published'))
            );

            $response = new JsonResponse([
                'status' => 'ok',
                'data' => $products->toArray()
            ]);
        } catch (InvalidStatusValueException $e) {
            $response = new JsonResponse([
                'status' => 'error',
                'errorMesage' => 'Invalid status value. Must be published, pending or removed'
            ], 500);
        }

        return $response;
    }
}