<?php


namespace App\Siroko\Api\Infrastructure\Ui\Http\Controller\Carts;


use App\Siroko\Api\Application\Command\AddProductToCartHandler;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddProductController
{
    private AddProductToCartHandler $addProductToCartHandler;

    public function __construct(AddProductToCartHandler $addProductToCartHandler)
    {
        $this->addProductToCartHandler = $addProductToCartHandler;
    }

    public function __invoke(Request $request)
    {
        try {
            $product = ($this->addProductToCartHandler)(new GetProductRequest(
                $request->get('uid')
            ));

            $response = new JsonResponse([
                'status' => 'ok',
                'data' => $product->toArray()
            ]);
        } catch (Exception $e) {
            $response = new JsonResponse([
                'status' => 'error',
                'errorMesage' => $e->getMessage()
            ], 500);
        }

        return $response;
    }
}