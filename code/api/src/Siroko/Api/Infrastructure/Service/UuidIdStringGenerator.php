<?php


namespace App\Siroko\Api\Infrastructure\Service;


use Ramsey\Uuid\Uuid;
use App\Siroko\Api\Domain\Service\IdStringGenerator;

class UuidIdStringGenerator implements IdStringGenerator
{
    public function generate(): string
    {
        return Uuid::uuid4();
    }

}