<?php

namespace App\Siroko\Api\Infrastructure\DataFixtures;

use App\Siroko\Api\Domain\Model\Products\ProductId;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;
use App\Siroko\Api\Domain\Service\IdStringGenerator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private ProductRepository $productRepository;
    private IdStringGenerator $idStringGenerator;

    public function __construct(ProductRepository $productRepository, IdStringGenerator $idStringGenerator)
    {
        $this->productRepository = $productRepository;
        $this->idStringGenerator = $idStringGenerator;
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 20; $i++) {
            $product = \App\Siroko\Api\Domain\Model\Products\Product::create(
                new ProductId($this->idStringGenerator->generate()),
                'title '.$i,
                'description '.$i,
                'published',
            );

            $this->productRepository->insert($product);
        }
    }
}
