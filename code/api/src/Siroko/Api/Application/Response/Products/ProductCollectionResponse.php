<?php


namespace App\Siroko\Api\Application\Response\Products;


use App\Siroko\Api\Domain\Model\Products\ProductCollection;

class ProductCollectionResponse
{
    private array $products;

    public function __construct(ProductCollection $productCollection)
    {
        $this->products = [];
        foreach ($productCollection->getCollection() as $product) {
            $this->products[] = new ProductResponse($product);
        }
    }

    public function getProducts(): array
    {
        return $this->products;
    }


    public function toArray()
    {
        return array_map(function ($product) {
            return $product->toArray();
        }, $this->getProducts());
    }
}