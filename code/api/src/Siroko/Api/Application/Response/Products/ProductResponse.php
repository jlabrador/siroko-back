<?php


namespace App\Siroko\Api\Application\Response\Products;


use App\Siroko\Api\Domain\Model\Products\Product;

class ProductResponse
{
    private string $id;
    private string $title;
    private string $description;
    private string $status;
    private int $numberCart;

    public function __construct(Product $product)
    {
        $this->id = $product->getId()->getValue();
        $this->title = $product->getTitle();
        $this->description =  $product->getDescription();
        $this->status = $product->getStatus()->getValue();
        $this->numberCart = $product->getNumberCart();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getNumberCart(): int
    {
        return $this->numberCart;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'status' => $this->getStatus(),
            'numberCart' => $this->getNumberCart(),
        ];
    }
}