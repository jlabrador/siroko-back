<?php

namespace App\Siroko\Api\Application\Query\Products;

use App\Siroko\Api\Application\Request\Products\GetProductsRequest;
use App\Siroko\Api\Application\Response\Products\ProductCollectionResponse;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;
use App\Siroko\Api\Domain\Model\Products\Status;

class GetProductsHandler
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(GetProductsRequest $productsRequest)
    {
        $products = $this->productRepository->findByStatus(
            new Status($productsRequest->getStatus())
        );

        return new ProductCollectionResponse($products);
    }

}