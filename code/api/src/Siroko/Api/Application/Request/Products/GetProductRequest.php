<?php

namespace App\Siroko\Api\Application\Request\Products;

class GetProductRequest
{
    private string $uid;

    public function __construct(string $uid)
    {
        $this->uid = $uid;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

}