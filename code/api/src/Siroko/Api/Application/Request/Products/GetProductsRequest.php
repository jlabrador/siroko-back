<?php

namespace App\Siroko\Api\Application\Request\Products;

class GetProductsRequest
{
    private string $status;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function getStatus(): string
    {
        return $this->status;
    }


}