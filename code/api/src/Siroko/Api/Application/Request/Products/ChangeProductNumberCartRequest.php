<?php

namespace App\Siroko\Api\Application\Request\Products;

class ChangeProductNumberCartRequest
{
    private string $uid;
    private int $valueNumberCart;

    public function __construct(string $uid, int $value)
    {
        $this->uid = $uid;
        $this->valueNumberCart = $value;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getValueNumberCart(): int
    {
        return $this->valueNumberCart;
    }



}