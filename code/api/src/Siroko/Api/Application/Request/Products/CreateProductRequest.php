<?php


namespace App\Siroko\Api\Application\Request\Products;


class CreateProductRequest
{
    public function __construct(
        private string $title,
        private string $description,
        private string $status,
    )
    {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

}