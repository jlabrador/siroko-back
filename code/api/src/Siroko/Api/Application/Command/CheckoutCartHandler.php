<?php


namespace App\Siroko\Api\Application\Command;


use App\Siroko\Api\Application\Request\Products\CreateProductRequest;
use App\Siroko\Api\Application\Request\Products\GetProductRequest;
use App\Siroko\Api\Application\Response\Products\ProductCollectionResponse;
use App\Siroko\Api\Application\Response\Products\ProductResponse;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\ProductId;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;
use App\Siroko\Api\Domain\Model\Products\Status;
use App\Siroko\Api\Domain\Service\IdStringGenerator;

class CheckoutCartHandler
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    public function __invoke()
    {
        $products = $this->productRepository->checkoutCart();

        return new ProductCollectionResponse($products);
    }

}