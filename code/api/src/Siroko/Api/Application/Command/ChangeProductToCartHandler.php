<?php


namespace App\Siroko\Api\Application\Command;


use App\Siroko\Api\Application\Request\Products\ChangeProductNumberCartRequest;
use App\Siroko\Api\Application\Response\Products\ProductResponse;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;

class ChangeProductToCartHandler
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    public function __invoke(ChangeProductNumberCartRequest $changeProductNumberCartRequest): ProductResponse
    {
        $product = $this->productRepository->changeNumberCart(
            $changeProductNumberCartRequest->getUid(),
            $changeProductNumberCartRequest->getValueNumberCart()
        );
        return new ProductResponse($product);
    }

}