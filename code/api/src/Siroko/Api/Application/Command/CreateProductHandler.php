<?php


namespace App\Siroko\Api\Application\Command;


use App\Siroko\Api\Application\Request\Products\CreateProductRequest;
use App\Siroko\Api\Application\Response\Products\ProductResponse;
use App\Siroko\Api\Domain\Model\Products\Product;
use App\Siroko\Api\Domain\Model\Products\ProductId;
use App\Siroko\Api\Domain\Model\Products\ProductRepository;
use App\Siroko\Api\Domain\Service\IdStringGenerator;

class CreateProductHandler
{
    private ProductRepository $productRepository;
    private IdStringGenerator $idStringGenerator;

    public function __construct(ProductRepository $productRepository, IdStringGenerator $idStringGenerator)
    {
        $this->productRepository = $productRepository;
        $this->idStringGenerator = $idStringGenerator;
    }


    public function __invoke(CreateProductRequest $createProductRequest): ProductResponse
    {
        $product = Product::create(
            new ProductId($this->idStringGenerator->generate()),
            $createProductRequest->getTitle(),
            $createProductRequest->getDescription(),
            $createProductRequest->getStatus(),
        );

        $this->productRepository->insert($product);

        return new ProductResponse($product);
    }

}