<?php

namespace App\Siroko\Api\Domain\Model\Products;

interface ProductRepository
{
    public function findById(string $uid): Product;
    public function findByStatus(Status $status): ProductCollection;
    public function insert(Product $product): void;
    public function addNumberCart(string $uid): Product;
    public function changeNumberCart(string $uid, int $value): Product;
    public function removeNumberCart(string $uid): Product;
    public function checkoutCart(): ProductCollection;

}