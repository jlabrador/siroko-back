<?php

namespace App\Siroko\Api\Domain\Model\Products;

class Status
{
    private string $value;

    const PUBLISHED = 'published';
    const PENDING = 'pending';
    const REMOVED = 'removed';

    const ALLOWED_VALUES = [
        self::PUBLISHED,
        self::PENDING,
        self::REMOVED
    ];

    public function __construct(string $value)
    {
        if (!in_array($value, self::ALLOWED_VALUES)) {
            throw new InvalidStatusValueException();
        }

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Status $status)
    {
        return $this->getValue() === $status->getValue();
    }

    public static function makePublished(): self
    {
        return new self(self::PUBLISHED);
    }

    public static function makePending(): self
    {
        return new self(self::PENDING);
    }

    public static function makeRemoved(): self
    {
        return new self(self::REMOVED);
    }
}