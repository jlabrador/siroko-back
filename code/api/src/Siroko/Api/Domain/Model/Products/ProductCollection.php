<?php

namespace App\Siroko\Api\Domain\Model\Products;

use App\Siroko\Api\Domain\Collection\ObjectCollection;

class ProductCollection extends ObjectCollection
{
    protected function className(): string
    {
        return Product::class;
    }

}