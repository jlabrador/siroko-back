<?php

namespace App\Siroko\Api\Domain\Model\Products;

class Product
{
    private ProductId $id;
    private string $title;
    private string $description;
    private Status $status;
    private int $numberCart;
    private \DateTimeInterface $createdAt;
    private ?\DateTimeInterface $updatedAt;

    private function __construct(
    )
    {
    }

    public static function create(
        ProductId $id,
        string $title,
        string $description,
        string $status,
    ): self
    {
        $product = new Product();

        $product->id = $id;
        $product->title = $title;
        $product->description = $description;
        $product->numberCart = 0;
        $product->status = new Status($status);
        $product->createdAt = new \DateTimeImmutable();
        $product->updatedAt = null;

        return $product;
    }

    public static function fromPrimitive(
        string $id,
        string $title,
        string $description,
        string $status,
        int $numberCart,
        \DateTimeInterface $createdAt,
        ?\DateTimeInterface $updatedAt,
    ): self
    {
        $product = new Product();

        $product->id = new ProductId($id);
        $product->title = $title;
        $product->description = $description;
        $product->status = new Status($status);
        $product->numberCart = $numberCart;
        $product->createdAt = $createdAt;
        $product->updatedAt = $updatedAt;

        return $product;
    }

    /**
     * @return ProductId
     */
    public function getId(): ProductId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getNumberCart(): int
    {
        return $this->numberCart;
    }
}