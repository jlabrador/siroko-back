<?php


namespace App\Siroko\Api\Domain\Service;


interface IdStringGenerator
{
    public function generate(): string;
}